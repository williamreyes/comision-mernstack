const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let documentoSchema = new Schema({
  Title: {
    type: String
  },
  Subject: {
    type: String
  },
  Description: {
    type: String
  },
  Source: {
    type: String
  },
  Type: {
    type: String
  },
  Coverage: {
    type: String
  }
}, {
    collection: 'documentos'
  })

module.exports = mongoose.model('Documento', documentoSchema)
