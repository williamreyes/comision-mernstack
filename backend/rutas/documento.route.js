let mongoose = require('mongoose'),
  express = require('express'),
  router = express.Router();

// Modelo documento
let documentoSchema = require('../modelos/Documento');

// CREAR documento
router.route('/crear-documento').post((req, res, next) => {
  documentoSchema.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      console.log(data)
      res.json(data)
    }
  })
});

// LEER documentos
router.route('/').get((req, res) => {
  documentoSchema.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Obtener documento sencillo
router.route('/editar-documento/:id').get((req, res) => {
  documentoSchema.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})


// Actualizar documento
router.route('/actualizar-documento/:id').put((req, res, next) => {
  documentoSchema.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('Documento actualizado Satisfactoriamente !')
    }
  })
})

// Borrar documento
router.route('/borrar-documento/:id').delete((req, res, next) => {
  documentoSchema.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = router;
