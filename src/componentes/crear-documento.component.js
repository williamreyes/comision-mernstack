import React, {Component} from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import axios from 'axios';

export default class CrearDocumento extends Component {

  constructor(props) {
    super(props)

    // Setting up functions
    this.onChangeDocumentoTitulo = this.onChangeDocumentoTitulo.bind(this);
    this.onChangeDocumentoClaves = this.onChangeDocumentoClaves.bind(this);
    this.onChangeDocumentoDescripcion = this.onChangeDocumentoDescripcion.bind(this);
    this.onChangeDocumentoFuente = this.onChangeDocumentoFuente.bind(this);
    this.onChangeDocumentoTipo = this.onChangeDocumentoTipo.bind(this);
    this.onChangeDocumentoCobertura = this.onChangeDocumentoCobertura.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // Setting up state
    this.state = {
      Title: '',
      Subject: '',
      Description: '',
      Source: '',
      Type: '',
      Coverage: ''
    }
  }

  onChangeDocumentoTitulo(e) {
    this.setState({Title: e.target.value})
  }

  onChangeDocumentoClaves(e) {
    this.setState({Subject: e.target.value})
  }

  onChangeDocumentoDescripcion(e) {
    this.setState({Description: e.target.value})
  }

  onChangeDocumentoFuente(e) {
    this.setState({Source: e.target.value})
  }

  onChangeDocumentoTipo(e) {
    this.setState({Type: e.target.value})
  }

  onChangeDocumentoCobertura(e) {
    this.setState({Coverage: e.target.value})
  }

  onSubmit(e) {
    e.preventDefault()

    const documentoObject = {
       Title: this.state.Title,
       Subject: this.state.Subject,
       Description: this.state.Description,
       Source: this.state.Source,
       Type: this.state.Type,
       Coverage: this.state.Coverage
     };

     axios.post('http://localhost:4000/documentos/crear-documento', documentoObject)
       .then(res => console.log(res.data));
  /*  console.log(`Documento creado Satisfactoriamente!`);
    console.log(`Title: ${this.state.Title}`);
    console.log(`Subject: ${this.state.Subject}`);
    console.log(`Description: ${this.state.Description}`);
    console.log(`Source: ${this.state.Source}`);
    console.log(`Type: ${this.state.Type}`);
    console.log(`Coverage: ${this.state.Coverage}`);*/

    this.setState({Title: '', Subject: '', Description: '', Source: '', Type: '', Coverage: ''})
  }

  render() {
    return (<div class="form-wrapper">
    <Form onSubmit={this.onSubmit}>
      <Form.Group controlId="Title">
        <Form.Label>Titulo</Form.Label>
        <Form.Control type="text" value={this.state.Title} onChange={this.onChangeDocumentoTitulo} />
      </Form.Group>

      <Form.Group controlId="Subject">
        <Form.Label>Claves</Form.Label>
        <Form.Control type="text" value={this.state.Subject} onChange={this.onChangeDocumentoClaves} />
      </Form.Group>

      <Form.Group controlId="Description">
        <Form.Label>Descripción</Form.Label>
        <Form.Control type="text" value={this.state.Description} onChange={this.onChangeDocumentoDescripcion} />
      </Form.Group>

      <Form.Group controlId="Source">
        <Form.Label>Fuente</Form.Label>
        <Form.Control type="text" value={this.state.Source} onChange={this.onChangeDocumentoFuente} />
      </Form.Group>

      <Form.Group controlId="Type">
        <Form.Label>Tipo</Form.Label>
        <Form.Control type="text" value={this.state.Type} onChange={this.onChangeDocumentoTipo} />
      </Form.Group>

      <Form.Group controlId="Coverage">
        <Form.Label>Cobertura</Form.Label>
        <Form.Control type="text" value={this.state.Coverage} onChange={this.onChangeDocumentoCobertura} />
      </Form.Group>

      <Button variant="danger" size="lg" block="block" type="submit">
        Crear Documento
      </Button>
    </Form>
    </div>);
  }
}
