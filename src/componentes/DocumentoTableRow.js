import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Button from 'react-bootstrap/Button';

export default class DocumentoTableRow extends Component {

    constructor(props) {
         super(props);
         this.borrarDocumento = this.borrarDocumento.bind(this);
    }

    borrarDocumento() {
         axios.delete('http://localhost:4000/documentos/borrar-documento/' + this.props.obj._id)
             .then((res) => {
                 console.log('Documento borrado satisfactoriamente!')
             }).catch((error) => {
                 console.log(error)
             })
    }

    render() {
        return (
            <tr>
                <td>{this.props.obj.Title}</td>
                <td>{this.props.obj.Subject}</td>
                <td>{this.props.obj.Description}</td>
                <td>{this.props.obj.Source}</td>
                <td>{this.props.obj.Type}</td>
                <td>{this.props.obj.Coverage}</td>
                <td>
                    <Link className="edit-link" to={"/editar-documento/" + this.props.obj._id}>
                        Editar
                    </Link>
                    <Button onClick={this.borrarDocumento} size="sm" variant="danger">Borrar</Button>
                </td>
            </tr>
        );
    }
}
