import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import axios from 'axios';

export default class EditarDocumento extends Component {

  constructor(props) {
    super(props)

    this.onChangeDocumentoTitulo = this.onChangeDocumentoTitulo.bind(this);
    this.onChangeDocumentoClaves = this.onChangeDocumentoClaves.bind(this);
    this.onChangeDocumentoDescripcion = this.onChangeDocumentoDescripcion.bind(this);
    this.onChangeDocumentoFuente = this.onChangeDocumentoFuente.bind(this);
    this.onChangeDocumentoTipo = this.onChangeDocumentoTipo.bind(this);
    this.onChangeDocumentoCobertura = this.onChangeDocumentoCobertura.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // State
    this.state = {
      Title: '',
      Subject: '',
      Description: '',
      Source: '',
      Type: '',
      Coverage: ''
    }
  }

  componentDidMount() {
    axios.get('http://localhost:4000/documentos/editar-documento/' + this.props.match.params.id)
      .then(res => {
        this.setState({
          Title: res.data.Title,
          Subject: res.data.Subject,
          Description: res.data.Description,
          Source: res.data.Source,
          Type: res.data.Type,
          Coverage: res.data.Coverage
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  onChangeDocumentoTitulo(e) {
    this.setState({ Title: e.target.value })
  }

  onChangeDocumentoClaves(e) {
    this.setState({ Subject: e.target.value })
  }

  onChangeDocumentoDescripcion(e) {
    this.setState({ Description: e.target.value })
  }

  onChangeDocumentoFuente(e) {
    this.setState({ Source: e.target.value })
  }

  onChangeDocumentoTipo(e) {
    this.setState({ Type: e.target.value })
  }

  onChangeDocumentoCobertura(e) {
    this.setState({ Coverage: e.target.value })
  }

  onSubmit(e) {
    e.preventDefault()

    const documentoObject = {
       Title: this.state.Title,
       Subject: this.state.Subject,
       Description: this.state.Description,
       Source: this.state.Source,
       Type: this.state.Type,
       Coverage: this.state.Coverage
     };

    axios.put('http://localhost:4000/documentos/actualizar-documento/' + this.props.match.params.id, documentoObject)
      .then((res) => {
        console.log(res.data)
        console.log('Documento actualizado satisfactoriamente')
      }).catch((error) => {
        console.log(error)
      })

    // Redirect to Student List
    this.props.history.push('/documento-listar')
  }


  render() {
    return (<div className="form-wrapper">
      <Form onSubmit={this.onSubmit}>
        <Form.Group controlId="Title">
          <Form.Label>Titulo</Form.Label>
          <Form.Control type="text" value={this.state.Title} onChange={this.onChangeDocumentoTitulo} />
        </Form.Group>

        <Form.Group controlId="Subject">
          <Form.Label>Claves</Form.Label>
          <Form.Control type="text" value={this.state.Subject} onChange={this.onChangeDocumentoClaves} />
        </Form.Group>

        <Form.Group controlId="Description">
          <Form.Label>Descripcion</Form.Label>
          <Form.Control type="text" value={this.state.Description} onChange={this.onChangeDocumentoDescripcion} />
        </Form.Group>

        <Form.Group controlId="Source">
          <Form.Label>Fuente</Form.Label>
          <Form.Control type="text" value={this.state.Source} onChange={this.onChangeDocumentoFuente} />
        </Form.Group>

        <Form.Group controlId="Type">
          <Form.Label>Tipo</Form.Label>
          <Form.Control type="text" value={this.state.Type} onChange={this.onChangeDocumentoTipo} />
        </Form.Group>

        <Form.Group controlId="Coverage">
          <Form.Label>Cobertura</Form.Label>
          <Form.Control type="text" value={this.state.Coverage} onChange={this.onChangeDocumentoCobertura} />
        </Form.Group>

        <Button variant="danger" size="lg" block="block" type="submit">
          Actualizar Documento
        </Button>
      </Form>
    </div>);
  }
}
