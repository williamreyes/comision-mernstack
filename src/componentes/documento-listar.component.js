import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import StudentTableRow from './DocumentoTableRow';

export default class DocumentoListar extends Component {

  constructor(props) {
    super(props)
    this.state = {
      documentos: []
    };
  }

  componentDidMount() {
    axios.get('http://localhost:4000/documentos/')
      .then(res => {
        this.setState({
          documentos: res.data
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  DataTable() {
    return this.state.documentos.map((res, i) => {
      return <StudentTableRow obj={res} key={i} />;
    });
  }

  render() {
    return (<div className="table-wrapper">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Titulo</th>
            <th>Claves</th>
            <th>Descripcion</th>
            <th>Fuente</th>
            <th>Tipo</th>
            <th>Cobertura</th>
          </tr>
        </thead>
        <tbody>
          {this.DataTable()}
        </tbody>
      </Table>
    </div>);
  }
}
