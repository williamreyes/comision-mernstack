import React from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "bootstrap/dist/css/bootstrap.css";
import "./App.css";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import CrearDocumento from "./componentes/crear-documento.component";
import EditarDocumento from "./componentes/editar-documento.component";
import DocumentoListar from "./componentes/documento-listar.component";

function App() {
  return (<Router>
    <div className="App">
      <header className="App-header">
        <Navbar bg="dark" variant="dark">
          <Container>

            <Navbar.Brand>
              <Link to={"/crear-documento"} className="nav-link">
              Comisión de la Verdad React MERN Stack App
              </Link>
		  Elaborado por : Ing. William Reyes
            </Navbar.Brand>

            <Nav className="justify-content-end">
              <Nav>
                <Link to={"/crear-documento"} className="nav-link">
                  Crear Documento
                </Link>
              </Nav>

              {/* <Nav>
                <Link to={"/editar-Documento/:id"} className="nav-link">
                  Editar Documento
                </Link>
              </Nav> */}

              <Nav>
                <Link to={"/documento-listar"} className="nav-link">
                  Lista de Documentos
                </Link>
              </Nav>
            </Nav>

          </Container>
        </Navbar>
      </header>

      <Container>
        <Row>
          <Col md={12}>
            <div className="wrapper">
              <Switch>
                <Route exact path='/' component={CrearDocumento} />
                <Route path="/crear-documento" component={CrearDocumento} />
                <Route path="/editar-documento/:id" component={EditarDocumento} />
                <Route path="/documento-listar" component={DocumentoListar} />
              </Switch>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  </Router>);
}

export default App;
